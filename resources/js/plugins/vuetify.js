import Vue from 'vue'
import Vuetify from 'vuetify'
// import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'

// Translation provided by Vuetify (javascript)
import zhHant from 'vuetify/es5/locale/zh-Hant';
import en from 'vuetify/es5/locale/en';

const opts = {
    lang: {
        locales: { zhHant, en },
        current: 'en',
    },
    theme: {
        dark: false,
    },
}

export default new Vuetify(opts)