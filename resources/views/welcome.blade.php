<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>{{ config('app.name') }}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <v-app light v-cloak>
                
                <v-toolbar class="white">
                    <v-toolbar-title>
                        <v-icon>info</v-icon> {{ config('app.name') }}
                    </v-toolbar-title>
                    @if (Route::has('login') && ! Auth::check() )
                        <v-spacer></v-spacer>
                        <login-button action="{{ $action or null }}" ></login-button>
                        <register-button action="{{ $action or null }}" ></register-button>
                        <remember-password action="{{ $action or null }}"></remember-password>
                        <reset-password
                                action="{{ $action or null }}"
                                token="{{ $token or null }}"
                                email="{{ $email or null }}"></reset-password>
                    @endif
                </v-toolbar>
                <v-main>
                    <section>
                        <v-parallax height="600">
                            <v-layout
                                    column
                                    align-center
                                    justify-center
                                    class="white--text"
                            >
                                <h1 class="white--text mb-2 display-1 text-xs-center">Parallax Template</h1>
                                <div class="subheading mb-3 text-xs-center">Powered by Vuetify</div>
                                <v-btn
                                        class="blue lighten-2 mt-5"
                                        dark
                                        large
                                        href="/home"
                                >
                                    Get Started
                                </v-btn>
                            </v-layout>
                        </v-parallax>
                    </section>

                    <section>
                        <v-layout
                                column
                                wrap
                                class="my-5"
                                align-center
                        >
                            <v-flex xs12 sm4 class="my-3">
                                <div class="text-xs-center">
                                    <h2 class="headline">The best way to start developing</h2>
                                    <span class="subheading">
                        Cras facilisis mi vitae nunc
                    </span>
                                </div>
                            </v-flex>
                            <v-flex xs12>
                                <v-container grid-list-xl>
                                    <v-layout row wrap align-center>
                                        <v-flex xs12 md4>
                                            <v-card class="elevation-0 transparent">
                                                <v-card-text class="text-xs-center">
                                                    <v-icon x-large class="blue--text text--lighten-2">color_lens</v-icon>
                                                </v-card-text>
                                                <v-card-title primary-title class="layout justify-center">
                                                    <div class="headline text-xs-center">Material Design</div>
                                                </v-card-title>
                                                <v-card-text>
                                                    Cras facilisis mi vitae nunc lobortis pharetra. Nulla volutpat tincidunt ornare.
                                                    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                    Nullam in aliquet odio. Aliquam eu est vitae tellus bibendum tincidunt. Suspendisse potenti.
                                                </v-card-text>
                                            </v-card>
                                        </v-flex>
                                        <v-flex xs12 md4>
                                            <v-card class="elevation-0 transparent">
                                                <v-card-text class="text-xs-center">
                                                    <v-icon x-large class="blue--text text--lighten-2">flash_on</v-icon>
                                                </v-card-text>
                                                <v-card-title primary-title class="layout justify-center">
                                                    <div class="headline">Fast development</div>
                                                </v-card-title>
                                                <v-card-text>
                                                    Cras facilisis mi vitae nunc lobortis pharetra. Nulla volutpat tincidunt ornare.
                                                    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                    Nullam in aliquet odio. Aliquam eu est vitae tellus bibendum tincidunt. Suspendisse potenti.
                                                </v-card-text>
                                            </v-card>
                                        </v-flex>
                                        <v-flex xs12 md4>
                                            <v-card class="elevation-0 transparent">
                                                <v-card-text class="text-xs-center">
                                                    <v-icon x-large class="blue--text text--lighten-2">build</v-icon>
                                                </v-card-text>
                                                <v-card-title primary-title class="layout justify-center">
                                                    <div class="headline text-xs-center">Completely Open Sourced</div>
                                                </v-card-title>
                                                <v-card-text>
                                                    Cras facilisis mi vitae nunc lobortis pharetra. Nulla volutpat tincidunt ornare.
                                                    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                                    Nullam in aliquet odio. Aliquam eu est vitae tellus bibendum tincidunt. Suspendisse potenti.
                                                </v-card-text>
                                            </v-card>
                                        </v-flex>
                                    </v-layout>
                                </v-container>
                            </v-flex>
                        </v-layout>
                    </section>

                    <section>
                        <v-parallax height="380">
                            <v-layout column align-center justify-center>
                                <div class="headline white--text mb-3 text-xs-center">Web development has never been easier</div>
                                <em>Kick-start your application today</em>
                                <v-btn
                                        class="blue lighten-2 mt-5"
                                        dark
                                        large
                                        href="/home"
                                >
                                    Get Started
                                </v-btn>
                            </v-layout>
                        </v-parallax>
                    </section>

                    <section>
                        <v-container grid-list-xl>
                            <v-layout row wrap justify-center class="my-5">
                                <v-flex xs12 sm4>
                                    <v-card class="elevation-0 transparent">
                                        <v-card-title primary-title class="layout justify-center">
                                            <div class="headline">Company info</div>
                                        </v-card-title>
                                        <v-card-text>
                                            Cras facilisis mi vitae nunc lobortis pharetra. Nulla volutpat tincidunt ornare.
                                            Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                                            Nullam in aliquet odio. Aliquam eu est vitae tellus bibendum tincidunt. Suspendisse potenti.
                                        </v-card-text>
                                    </v-card>
                                </v-flex>
                                <v-flex xs12 sm4 offset-sm1>
                                    <v-card class="elevation-0 transparent">
                                        <v-card-title primary-title class="layout justify-center">
                                            <div class="headline">Contact us</div>
                                        </v-card-title>
                                        <v-card-text>
                                            Cras facilisis mi vitae nunc lobortis pharetra. Nulla volutpat tincidunt ornare.
                                        </v-card-text>
                                        
                                    </v-card>
                                </v-flex>
                            </v-layout>
                        </v-container>
                    </section>

                    <v-footer class="blue darken-2">
                        <v-layout row wrap align-center>
                            <v-flex xs12>
                                <div class="white--text ml-3">
                                    Made with
                                    <v-icon class="red--text">favorite</v-icon>
                                    by <a class="white--text" href="https://vuetifyjs.com" target="_blank">Vuetify</a>
                                    and <a class="white--text" href="https://github.com/vwxyzjn">Costa Huang</a>
                                </div>
                            </v-flex>
                        </v-layout>
                    </v-footer>
                </v-main>
            </v-app>
        </div>
        <script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>
    </body>
</html>
